<?php
/**
 * Plugin Name: TCU Timeline Plugin
 * Description: Easily add a timeline into any page through a shortcode.
 * Version: 1.0.0
 * Author: Website and Social Media Management
 * Author URI: http://mkc.tcu.edu/web-management.asp
 * Text Domain: tcu-timeline-plugin
 *
 * License: GNU General Public License v2.0
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @package tcu_timeline_plugin
 * @since TCU Timeline Plugin 1.0.0
 */

/**
 * Exit if accessed directly
 **/
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Let's get started!
 */
if ( class_exists( 'Tcu_Timeline_Plugin' ) ) {
	new Tcu_Timeline_Plugin();
}

/**
 * Registers plugin and loads functionality
 *
 * @author Mayra Perales <m.j.perales@tcu.edu>
 */
class Tcu_Timeline_Plugin {

	/**
	 * Our plugin version
	 *
	 * @var string Plugin version number.
	 */
	protected static $version = '1.0.0';

	/**
	 * Display Timeline Object
	 *
	 * @var object The Display Timeline Object
	 */
	protected $display_timeline;

	/**
	 * Create Timeline Object
	 *
	 * @var object The Create Timeline Object
	 */
	protected $create_timeline;

	/**
	 * Create Update Timeline Object
	 *
	 * @var object The pdate Timeline Object
	 */
	protected $update_timeline;

	/**
	 * Our constructor
	 */
	public function __construct() {

		// Loads all our include files.
		$this->load_files();

		// Instatiate Tcu_Create_Timeline.
		$this->create_timeline();

		// Instatiate Tcu_Display_Timeline.
		$this->display_timeline();

		// Instatiate Tcu_Update_Timeline.
		$this->update_timeline();

		// Activation and uninstall hooks.
		register_activation_hook( __FILE__, array( &$this, 'activate_me' ) );
		register_deactivation_hook( __FILE__, array( __CLASS__, 'uninstall_me' ) );

		// Load all our css and js files on front end side.
		add_action( 'wp_enqueue_scripts', array( $this, 'register_front_end_scripts' ), 999 );
	}

	/**
	 * Return plugin version
	 *
	 * @return string $version Plugin version
	 */
	public static function get_version() {
			return self::$version;
	}

	/**
	 * Instantiate Tcu_Create_Timeline
	 *
	 * @return object $create_timeline Timeline Custom Post Type
	 **/
	public function create_timeline() {
		if ( ! $this->create_timeline ) {
			$this->create_timeline = new Tcu_Create_Timeline();
		}
		return $this->create_timeline;
	}

	/**
	 * Instantiate Tcu_Display_Timeline
	 *
	 * @return object $display_timeline Timeline Display Object
	 **/
	public function display_timeline() {
		if ( ! $this->display_timeline ) {
			$this->display_timeline = new Tcu_Display_Timeline();
		}
		return $this->display_timeline;
	}

	/**
	 * Instantiate Tcu_Update_Timeline
	 *
	 * @return object $display_timeline Timeline Update Object
	 **/
	public function update_timeline() {
		if ( ! $this->update_timeline ) {
			$this->update_timeline = new Tcu_Update_Timeline();
		}
		return $this->update_timeline;
	}

	/**
	 * Activate our plugin
	 * Check if TCU Web Standards theme is installed
	 **/
	public function activate_me() {

		// Register Post Type.
		$this->create_timeline->register_timeline_post_type();

		// Set user capabilities.
		Tcu_Create_Timeline::set_user_caps();

		// Flush our rewrites.
		flush_rewrite_rules();
	}

	/**
	 * Deactivate plugin
	 */
	public function uninstall_me() {

		// Remove user capabilities.
		Tcu_Create_Timeline::remove_all_caps();

		// Flush our rewrites.
		flush_rewrite_rules();
	}

	/**
	 * Load all our dependencies
	 */
	public function load_files() {
		require_once 'library/classes/class-tcu-display-timeline.php';
		require_once 'library/classes/class-tcu-create-timeline.php';
		require_once 'library/classes/class-tcu-update-timeline.php';
		require_once 'library/inc/acf-timeline-fields.php';
	}

	/**
	 * Register our front end scripts
	 * only if TCU Web Standards theme
	 * is not installed.
	 **/
	public function register_front_end_scripts() {

		wp_register_style( 'tcu-timeline-styles', plugins_url( 'library/css/style.min.css', __FILE__ ), array(), self::$version, 'all' );
		wp_register_script( 'tcu-timeline-scripts', plugins_url( 'library/js/min/main.min.js', __FILE__ ), array( 'jquery', 'jquery-ui-sortable' ), self::$version, true );

		wp_enqueue_style( 'tcu-timeline-styles' );
		wp_enqueue_script( 'tcu-timeline-scripts' );

	}
}
