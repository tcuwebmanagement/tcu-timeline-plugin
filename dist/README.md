# TCU Timeline Plugin

## Easily add a timeline to any page through the WP Visual Editor.

This plugin uses the Vertical Timeline from [Cody House](https://codyhouse.co/gem/vertical-timeline/).

### Enjoy!
