<?php
/**
 * Timeline ACF fields
 *
 * @package tcu_timeline_plugin
 * @since TCU Timeline Plugin 1.0.0
 */

/**
 * Exit if accessed directly
 **/
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

add_action( 'acf/init', 'tcu_timeline_fiels' );

/**
 * Callback function for ACF/INIT action
 */
function tcu_timeline_fiels() {

	acf_add_local_field_group(
		array(
			'key'                   => 'group_5acd1eb0e569e',
			'title'                 => 'Timeline',
			'fields'                => array(
				array(
					'key'               => 'field_5ace2ec8b14d0',
					'label'             => 'Timeline Style',
					'name'              => '',
					'type'              => 'tab',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'placement'         => 'top',
					'endpoint'          => 0,
				),
				array(
					'key'               => 'field_5ace2eebb14d1',
					'label'             => 'Style',
					'name'              => 'tcu_timeline_style',
					'type'              => 'radio',
					'instructions'      => 'Please choose between the plain or numbered option. This selection either adds a number to the circle or it leaves it blank.',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'choices'           => array(
						'Plain'    => 'Plain',
						'Numbered' => 'Numbered',
					),
					'allow_null'        => 0,
					'other_choice'      => 0,
					'save_other_choice' => 0,
					'default_value'     => 'Plain',
					'layout'            => 'horizontal',
					'return_format'     => 'value',
				),
				array(
					'key'               => 'field_5acd1ecfc34b4',
					'label'             => 'Timeline Item',
					'name'              => 'tcu_timeline_item',
					'type'              => 'repeater',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'collapsed'         => 'field_5acd1f00c34b5',
					'min'               => 0,
					'max'               => 0,
					'layout'            => 'block',
					'button_label'      => '+ Add Timeline Item',
					'sub_fields'        => array(
						array(
							'key'               => 'field_5acd1f88c34ba',
							'label'             => 'Content',
							'name'              => '',
							'type'              => 'tab',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'placement'         => 'top',
							'endpoint'          => 0,
						),
						array(
							'key'               => 'field_5ace2c83f0480',
							'label'             => 'Color',
							'name'              => 'tcu_timeline_color',
							'type'              => 'radio',
							'instructions'      => 'Select the background color of the circle in the timeline track.',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'choices'           => array(
								'#A3A9AC' => 'Horned Frog Grey',
								'#F47D20' => 'Orange',
								'#F9D44B' => 'Yellow',
								'#95DACF' => 'Aqua',
								'#4D1979' => 'Purple',
							),
							'allow_null'        => 0,
							'other_choice'      => 0,
							'save_other_choice' => 0,
							'default_value'     => 'Purple',
							'layout'            => 'horizontal',
							'return_format'     => 'value',
						),
						array(
							'key'               => 'field_5acd1f00c34b5',
							'label'             => 'Title',
							'name'              => 'tcu_timeline_title',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => '',
						),
						array(
							'key'               => 'field_5acd2099b0596',
							'label'             => 'Date',
							'name'              => 'tcu_timeline_date',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => '',
						),
						array(
							'key'               => 'field_5acd1f14c34b6',
							'label'             => 'Description',
							'name'              => 'tcu_timeline_description',
							'type'              => 'wysiwyg',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'tabs'              => 'all',
							'toolbar'           => 'full',
							'media_upload'      => 1,
							'delay'             => 0,
						),
						array(
							'key'               => 'field_5acd1f40c34b7',
							'label'             => 'Link Text',
							'name'              => 'tcu_timeline_link_text',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => '',
						),
						array(
							'key'               => 'field_5acd1f55c34b8',
							'label'             => 'Link',
							'name'              => 'tcu_timeline_link',
							'type'              => 'url',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
						),
						array(
							'key'               => 'field_5acd1f65c34b9',
							'label'             => 'Aria-label',
							'name'              => 'tcu_timeline_aria_label',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => '',
						),
						array(
							'key'               => 'field_5acd1fc9c34bc',
							'label'             => 'Text inside circle',
							'name'              => 'tcu_timeline_text',
							'type'              => 'number',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => array(
								array(
									array(
										'field'    => 'field_5ace2eebb14d1',
										'operator' => '==',
										'value'    => 'Numbered',
									),
								),
							),
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => 1,
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'min'               => 1,
							'max'               => 9,
							'step'              => 1,
						),
					),
				),
			),
			'location'              => array(
				array(
					array(
						'param'    => 'post_type',
						'operator' => '==',
						'value'    => 'tcu_timeline',
					),
				),
			),
			'menu_order'            => 0,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => array(
				0 => 'the_content',
			),
			'active'                => 1,
			'description'           => '',
		)
	);

}
