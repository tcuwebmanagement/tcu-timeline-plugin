<?php
/**
 *  Display Timeline
 *
 * @package tcu_timeline_plugin
 * @since TCU Timeline Plugin 1.0.0
 */

/**
 * Exit if access directly
 **/
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Display our timeline
 *
 * @uses Tcu_Create_Timeline
 */
class Tcu_Display_Timeline {

	/**
	 * Our Constructor
	 */
	public function __construct() {

			// [tcu_timeline id="1"]
			add_shortcode( 'tcu_timeline', array( $this, 'create_shortcode' ) );

			// Print thickbox.
			add_action( 'admin_footer', array( $this, 'media_thickbox' ) );

			// Print media button.
			add_action( 'media_buttons', array( $this, 'media_button' ), 999 );
	}

	/**
	 * Let's get our object set as an array
	 *
	 * @param object $data Timeline object.
	 * @return array Timeline object in array
	 */
	public static function set_array( $data ) {
		foreach ( $data as $key => $value ) {
			$key = $value;
		}

		return $data;
	}

	/**
	 * Query tcu_timeline post type
	 *
	 * @param array $args Arguments to be merged with the post query.
	 * @return object WP_Query Post query
	 */
	public static function query_timeline( $args = array() ) {
		$timeline = array();

		// Our default query arguments.
		$defaults = array(
			'posts_per_page' => -1,
			'offset'         => 0,
			'post_status'    => 'publish',
			'post_type'      => Tcu_Create_Timeline::get_post_type_name(),
			'orderby'        => 'ID',
			'order'          => 'ASC',
			'no_found_rows'  => true,
		);

		// Merge the query arguments.
		$args = array_merge( (array) $defaults, (array) $args );

		// Execute the query.
		$query = get_posts( $args );

		return $query;
	}

	/**
	 * Get all our timelines
	 *
	 * @return array $output All our timelines
	 **/
	public static function all_timelines() {

		// Query our post type.
		$timelines = self::query_timeline();

		$output = array();

		// Loop through each timeline.
		foreach ( $timelines as $post ) {

			// Set array.
			$timeline = self::set_array( get_object_vars( $post ) );
			$output[] = $timeline;
		}

		wp_reset_postdata();

		return $output;
	}

	/**
	 * Grab our timeline by post ID
	 *
	 * @param int $id  The timeline/post ID.
	 * @return array $output Timeline by ID.
	 */
	public function get_by_id( $id ) {
		$post = get_post( $id );

		// Check if post exists.
		if ( ! $post ) {
			return false;
		}

		// Check if we are using the correct post type.
		if ( Tcu_Create_Timeline::get_post_type_name() !== $post->post_type ) {
			return false;
		}

		$output = array();

		// Set up our array.
		$timeline = self::set_array( get_object_vars( $post ) );
		$output[] = $timeline;

		return $output;
	}

	/**
	 * Make sure auto embed works inside our Visual Editor
	 *
	 * @param string $content  The content within each timeline post.
	 * @return string $content Embeded content
	 */
	public function auto_embed( $content ) {
		if ( isset( $GLOBALS['wp_embed'] ) ) {
			$content = $GLOBALS['wp_embed']->autoembed( $content );
		}

		return $content;
	}

	/**
	 * Render our Timelines's HTML
	 *
	 * @param array $timeline WP_Query of our timeline by ID.
	 * @return string $html The HTML shell
	 */
	public function render_html( $timeline ) {
		$key       = key( $timeline );
		$style     = get_field( 'tcu_timeline_style', $timeline[ $key ]['ID'] );
		$timelines = get_field( 'tcu_timeline_item', $timeline[ $key ]['ID'] );

		if ( $timelines ) {
			$html  = '<div class="cd-timeline js-cd-timeline">';
			$html .= '<div class="cd-timeline__container">';

			foreach ( $timelines as $timeline ) {

				$html .= '<div class="cd-timeline__block js-cd-block">';

				// Let's decide the style of our circle.
				if ( 'Plain' === $style ) {
					// Plain Circle.
					$html .= '<div style="background-color:' . $timeline['tcu_timeline_color'] . '; " class="cd-timeline__img js-cd-img"><span></span></div><!-- end of .cd-timeline__img -->';
				} else {
					// Numbered Circle.
					$html .= '<div style="background-color:' . $timeline['tcu_timeline_color'] . '; " class="cd-timeline__num js-cd-img">';
					$html .= '<span>' . $timeline['tcu_timeline_text'] . '</span>';
					$html .= '</div><!-- end of .cd-timeline__num -->';
				}

				// Timeline content.
				$html .= '<div class="cd-timeline__content js-cd-content">';
				$html .= '<h2>' . $timeline['tcu_timeline_title'] . '</h2>';
				$html .= '<div class="cd-timeline__editor">';
				$html .= do_shortcode( $timeline['tcu_timeline_description'] );

				// Timeline link.
				if ( $timeline['tcu_timeline_link'] ) {
					$html .= '<a href="' . $timeline['tcu_timeline_link'] . '" class="cd-timeline__read-more">' . $timeline['tcu_timeline_link_text'] . '</a>';
				} elseif ( $timeline['tcu_timeline_aria_label'] ) {
					$html .= '<a aria-label="' . $timeline['tcu_timeline_aria_label'] . '" href="' . $timeline['tcu_timeline_link'] . '" class="cd-timeline__read-more">' . $timeline['tcu_timeline_link_text'] . '</a>';
				}

				// Timeline Date.
				$html .= '<span class="cd-timeline__date">' . $timeline['tcu_timeline_date'] . '</span>';

				$html .= '</div><!-- end of .cd-timeline__editor -->';
				$html .= '</div></!-- end of .cd-timeline__content -->';

				$html .= '</div><!-- end of .cd-timeline__block -->';
			}

			$html .= '</div><!-- end of .cd-timeline__container -->';
			$html .= '</div><!-- end of .cd-timeline -->';
		}

		return $html;
	}

	/**
	 * Creates our shortcode
	 *
	 * @param array $atts The timeline ID.
	 * @return string $html Timeline shortcode
	 */
	public function create_shortcode( $atts ) {

		if ( ! empty( $atts['id'] ) ) {
			$timeline = self::get_by_id( $atts['id'] );
			$html     = self::render_html( $timeline );
		}

		return $html;
	}

	/**
	 * Prints our "ADD Timeline" Media Thickbox
	 *
	 * @return void
	 */
	public function media_thickbox() {
		global $pagenow;

		if ( ( 'post.php' || 'post-new.php' ) != $pagenow ) {
			return;
		}

		// Let's get all our timelines.
		$timelines = self::all_timelines(); ?>

		<style type="text/css">
			.section {
				padding: 15px 15px 0 15px;
			}
		</style>

		<script type="text/javascript">
		/**
		* Send shortcode to the editor
		*/
		var insertTimeline = function() {

			var id = jQuery( '#tcutimeline_thickbox' ).val();

			// alert if no timeline was selected
			if( '-1' === id ) {
				return alert( 'Please select a timeline from the dropdown menu.' );
			}

			// Send shortcode to editor
			send_to_editor( '[tcu_timeline id="' + id + '"]' );

			// close thickbox
			tb_remove();
		};
		</script>

		<style>
		.acf-field[data-name="tcu_timeline_description"] a.insert-timeline {
			display: none !important;
		}
		</style>
		<div id="select-tcu-timeline" style="display: none;">
			<div class="section">
				<h2><?php esc_html_e( 'Add a timeline', 'tcu-timeline-plugin' ); ?></h2>
				<span><?php esc_html_e( 'Select a timeline to insert from the box below.', 'tcu-timeline-plugin' ); ?></span>
			</div>

			<div class="section">
				<select name="tcu_timeline_name" id="tcutimeline_thickbox">
					<option value="-1">
						<?php esc_html_e( 'Select Timeline', 'tcu-timeline-plugin' ); ?>
					</option>
					<?php
					foreach ( $timelines as $timeline ) {
						echo "<option value=\"{$timeline['ID']}\">{$timeline['post_title']} (ID #{$timeline['ID']})</option>";
					}
					?>
				</select>
			</div>

			<div class="section">
				<button id="insert-timeline" class="button-primary" onClick="insertTimeline();"><?php esc_html_e( 'Insert Timeline', 'tcu-timeline-plugin' ); ?></button>
				<button id="close-timeline-thickbox" class="button-secondary" style="margin-left: 5px;" onClick="tb_remove();"><a><?php esc_html_e( 'Close', 'tcu-timeline-plugin' ); ?></a></button>
			</div>
		</div>
	<?php
	}

	/**
	 * Media button
	 *
	 * @param int $editor_id Editor ID.
	 */
	public function media_button( $editor_id ) {
	?>

		<style type="text/css">
			a.insert-timeline span.dashicons-feedback:before {
				content: "\f538";
				font: 400 16px/1 dashicons;
				speak: none;
				-webkit-font-smoothing: antialiased;
				-moz-osx-font-smoothing: grayscale;
			}
		</style>

		<a href="#TB_inline?width=480&amp;inlineId=select-tcu-timeline" class="button thickbox insert-timeline" data-editor="<?php echo esc_attr( $editor_id ); ?>" title="<?php echo esc_attr( 'Add Timeline', 'tcu-timeline-plugin' ); ?>">
			<span class="dashicons dashicons-feedback"></span><?php esc_html_e( ' Add Timeline', 'tcu-timeline-plugin' ); ?>
		</a>

	<?php
	}
}
