/*eslint no-undef: "error"*/
/*eslint-env node*/
module.exports = function( grunt ) {
    grunt.initConfig( {

        // Let's provide our users a minified version of all our CSS files
        cssmin: {
            target: {
                files: [
                    {
                        expand: true,
                        cwd: 'library/css/',
                        src: [ '*.css', '!*.min.css' ],
                        dest: 'library/css/',
                        ext: '.min.css'
                    }
                ]
            }
        },

        // Let's minimize our JS files
        uglify: {

            // Front-end
            main: {
                src: 'library/js/main.js',
                dest: 'library/js/min/main.min.js'
            }
        },

        // This creates a clean WP theme copy to use in production
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        src: [ './*.php', './CHANGELOG.md', './README.md', 'library/**' ],
                        dest: 'dist/'
                    }
                ]
            }
        },

        // Let's zip up our /dist (production wordpress plugin)
        // Change version number
        compress: {
            main: {
                options: {
                    archive: 'tcu-timeline-plugin.1.0.0.zip'
                },
                files: [
                    {
                        expand: true,
                        cwd: 'dist/',
                        src: [ '**' ],
                        dest: 'tcu-timeline-plugin/'
                    }
                ]
            }
        },

        browserSync: {
            dev: {
                bsFiles: {
                    src: 'library/css/style.css'
                },
                options: {
                    proxy: 'localhost/~mjperales/wordpress/'
                }
            }
        }
    } );

    grunt.loadNpmTasks( 'grunt-contrib-cssmin' );
    grunt.loadNpmTasks( 'grunt-contrib-uglify' );
    grunt.loadNpmTasks( 'grunt-contrib-uglify' );
    grunt.loadNpmTasks( 'grunt-contrib-copy' );
    grunt.loadNpmTasks( 'grunt-contrib-compress' );
    grunt.loadNpmTasks( 'grunt-browser-sync' );

    // min our CSS files
    grunt.registerTask( 'default', [ 'cssmin', 'uglify' ] );

    // Build copy to production directory
    grunt.registerTask( 'build', [ 'copy' ] );

    // Zip our clean directory to easily install in WP
    grunt.registerTask( 'zip', [ 'compress' ] );

    grunt.registerTask( 'watch', [ 'browserSync' ] );
};
