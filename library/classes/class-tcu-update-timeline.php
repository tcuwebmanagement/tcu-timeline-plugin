<?php
/**
 *  Update Timeline Plugin
 *
 * @package tcu_timeline_plugin
 * @since TCU Timeline Plugin 1.0.0
 */

/**
 * Exit if accessed directly
 **/
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Update plugin
 */
class Tcu_Update_Timeline {

	/**
	 * API URL
	 *
	 * @var string URL to connect to our downloads.
	 */
	private $api_url = 'https://webmanage.tcu.edu/api/';

	/**
	 * Plugin slug
	 *
	 * @var string Plugin slug name.
	 */
	private $plugin_slug = 'tcu-timeline-plugin';

	/**
	 * Our constructor
	 */
	public function __construct() {

		// Take over the update check.
		add_filter( 'pre_set_site_transient_update_plugins', array( $this, 'check_update' ) );

		// Take over the Plugin info screens.
		add_filter( 'plugins_api', array( $this, 'api_call' ), 10, 3 );
	}


	/**
	 * Check for updates
	 *
	 * @param mixed $transient Value of site transient. Expected to not be SQL-escaped.
	 * @return mixed $transient
	 */
	public function check_update( $transient ) {
		global $wp_version;

		if ( empty( $transient->checked ) ) {
			return $transient;
		}

		$args = array(
			'slug'    => $this->plugin_slug,
			'version' => $transient->checked[ $this->plugin_slug . '/' . $this->plugin_slug . '.php' ],
		);

		$request_string = array(
			'body'       => array(
				'action'  => 'basic_check',
				'request' => serialize( $args ),
				'api-key' => md5( get_bloginfo( 'url' ) ),
			),
			'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' ),
		);

		// Start checking for an update.
		$raw_response = wp_remote_post( $this->api_url, $request_string );

		if ( ! is_wp_error( $raw_response ) && ( 200 === $raw_response['response']['code'] ) ) {
			$response = unserialize( $raw_response['body'] );
		}

		if ( $response && is_object( $response ) && ! empty( $response ) ) {
			$transient->response[ $this->plugin_slug . '/' . $this->plugin_slug . '.php' ] = $response;
		}

		return $transient;
	}

	/**
	 * Display update information
	 *
	 * @param array  $results The result object or array. Default false.
	 * @param string $action  API action to perform: 'plugin_information'.
	 * @param mixed  $args    Array/object of arguments to serialize for the Plugin Info API.
	 */
	public function api_call( $results, $action, $args ) {
		global $wp_version;

		if ( ! isset( $args->slug ) || ( $args->slug !== $this->plugin_slug ) ) {
			return $results;
		}

		// Get the current version.
		$plugin_info     = get_site_transient( 'update_plugins' );
		$current_version = $plugin_info->checked[ $this->plugin_slug . '/' . $this->plugin_slug . '.php' ];
		$args->version   = $current_version;

		$request_string = array(
			'body'       => array(
				'action'  => $action,
				'request' => serialize( $args ),
				'api-key' => md5( get_bloginfo( 'url' ) ),
			),
			'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' ),
		);

		$request = wp_remote_post( $this->api_url, $request_string );

		if ( is_wp_error( $request ) ) {
			$results = new WP_Error( 'plugins_api_failed', __( 'An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>', 'tcu-timeline-plugin' ), $request->get_error_message() );
		} else {
			$results = unserialize( $request['body'] );

			if ( false === $results ) {
				$results = new WP_Error( 'plugins_api_failed', __( 'An unknown error occurred', 'tcu-timeline-plugin' ), $request['body'] );
			}
		}

		return $results;
	}
}
