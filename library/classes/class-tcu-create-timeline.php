<?php
/**
 *  Create Timeline
 *
 * @package tcu_timeline_plugin
 * @since TCU Timeline Plugin 1.0.0
 */

/**
 * Exit if access directly
 **/
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Registers our post type
 * Adds our custom meta boxes
 *
 * @author Mayra Perales <m.j.perales@tcu.edu>
 **/
class Tcu_Create_Timeline {

	/**
	 * Post type name
	 *
	 * @var string Post type name
	 */
	protected static $post_type_name = 'tcu_timeline';

	/**
	 * User capabilities
	 *
	 * @var array User capabilities
	 */
	private static $cap_aliases = array(
		// Full permissions to a post type.
		'editor'      => array(
			'read',
			'read_private_posts',
			'edit_posts',
			'edit_others_posts',
			'edit_private_posts',
			'edit_published_posts',
			'delete_posts',
			'delete_others_posts',
			'delete_private_posts',
			'delete_published_posts',
			'publish_posts',
		),
		'author'      => array(
			// Full permissions for content the user created.
			'read',
			'edit_posts',
			'edit_published_posts',
			'delete_posts',
			'delete_published_posts',
			'publish_posts',
		),
		'contributor' => array(
			// Create, but not publish.
			'read',
			'edit_posts',
			'delete_posts',
		),
		'subscriber'  => array(
			// Read only.
			'read',
		),
	);

	/**
	 * Our constructor
	 *
	 * @return void
	 */
	public function __construct() {

		// Register post type.
		add_action( 'init', array( &$this, 'register_timeline_post_type' ) );

		// Add user capabilities.
		add_action( 'wp_loaded', array( $this, 'set_user_caps' ) );

		// Replace table columns in edit.php.
		add_action( 'manage_tcu_timeline_posts_columns', array( $this, 'admin_columns' ), 10, 2 );

		// Add shortcode to table column in edit.php.
		add_action( 'manage_tcu_timeline_posts_custom_column', array( $this, 'shortcode_column' ), 10, 2 );
	}

	/**
	 * Combine and map our post meta arrays
	 *
	 * @param array $array Combined array from array_map().
	 * @return array $new associative Combined array with associative keys.
	 */
	public static function map_post_array( $array ) {
		$new    = array();
		$length = count( $array );

		for ( $i = 0; $i < $length; $i++ ) {
			$add_item = array(
				'title'   => $array[ $i ][0],
				'content' => $array[ $i ][1],
			);

			// Push our data into our new array.
			array_push( $new, $add_item );
		}

		// Return associative array.
		return $new;
	}

	/**
	 * Returns post type name
	 *
	 * @return $post_type_name string The name of the custom post type
	 */
	public static function get_post_type_name() {
		return self::$post_type_name;
	}

	/**
	 * Register our Post Type
	 *
	 * @return void
	 **/
	public function register_timeline_post_type() {
		register_post_type(
			self::$post_type_name,
			// Let's now add all the options for this post type.
			array(
				'labels'              => array(
					'name'               => __( 'Timelines', 'tcu-timeline-plugin' ),
					'singular_name'      => __( 'Timeline', 'tcu-timeline-plugin' ),
					'all_items'          => __( 'All Timelines', 'tcu-timeline-plugin' ),
					'add_new'            => __( 'Add New', 'tcu-timeline-plugin' ),
					'add_new_item'       => __( 'Add New Timeline', 'tcu-timeline-plugin' ),
					'edit'               => __( 'Edit', 'tcu-timeline-plugin' ),
					'edit_item'          => __( 'Edit Timeline', 'tcu-timeline-plugin' ),
					'new_item'           => __( 'New Timeline', 'tcu-timeline-plugin' ),
					'view_item'          => __( 'View Timeline', 'tcu-timeline-plugin' ),
					'search_items'       => __( 'Search Timelines', 'tcu-timeline-plugin' ),
					'not_found'          => __( 'Nothing found in the Database.', 'tcu-timeline-plugin' ),
					'not_found_in_trash' => __( 'Nothing found in Trash', 'tcu-timeline-plugin' ),
				),
				'description'         => __( 'Easily add a timeline to any page.', 'tcu-timeline-plugin' ),
				'public'              => false,
				'has_archive'         => false,
				'publicly_queryable'  => true,
				'show_ui'             => true,
				'supports'            => array( 'title', 'author' ),
				'menu_position'       => 20,
				'menu_icon'           => 'dashicons-layout',
				'exclude_from_search' => true,
				'capability_type'     => array( 'tcu_timeline', 'tcu-timeline-plugin' ),
				'map_meta_cap'        => true,
			)
		); /* end of register post type */
	}

	/**
	 * Grant caps for the given post type to the given role
	 *
	 * @param string $post_type The post type to grant caps for.
	 * @param string $role_id The role receiving the caps.
	 * @param string $level The capability level to grant (see the list of caps above).
	 *
	 * @return bool false if the action failed for some reason, otherwise true
	 */
	public static function register_post_type_caps( $post_type, $role_id, $level = '' ) {
		if ( empty( $level ) ) {
			$level = $role_id;
		}

		if ( 'administrator' === $level ) {
			$level = 'editor';
		}

		if ( ! isset( self::$cap_aliases[ $level ] ) ) {
			return false;
		}

		$role = get_role( $role_id );
		if ( ! $role ) {
			return false;
		}

		$pto = get_post_type_object( $post_type );
		if ( empty( $pto ) ) {
			return false;
		}

		foreach ( self::$cap_aliases[ $level ] as $alias ) {
			if ( isset( $pto->cap->$alias ) ) {
					$role->add_cap( $pto->cap->$alias );
			}
		}

		return true;
	}


	/**
	 * Grant caps for the given post type
	 */
	public static function set_user_caps() {
		foreach ( array( 'administrator', 'editor', 'author', 'contributor', 'subscriber' ) as $role ) {
			self::register_post_type_caps( self::$post_type_name, $role );
		}
	}

	/**
	 * Remove all caps for the given post type from the given role
	 *
	 * @param string $post_type The post type to remove caps for.
	 * @param string $role_id The role which is losing caps.
	 *
	 * @return bool false If the action failed for some reason, otherwise true.
	 */
	public static function remove_post_type_caps( $post_type, $role_id ) {

		$role = get_role( $role_id );
		if ( ! $role ) {
			return false;
		}

		foreach ( $role->capabilities as $cap => $has ) {
			if ( strpos( $cap, $post_type ) !== false ) {
				$role->remove_cap( $cap );
			}
		}

		return true;
	}

	/**
	 * Remove capabilities for events and related post types from default roles
	 *
	 * @return void
	 */
	public static function remove_all_caps() {
		foreach ( array( 'administrator', 'editor', 'author', 'contributor', 'subscriber' ) as $role ) {
			self::remove_post_type_caps( self::$post_type_name, $role );
		}
	}

	/**
	 * Replace table columns in edit.php
	 *
	 * @param array $columns Array of columns in edit.php.
	 * @return array $columns Updated array of columns in edit.php
	 */
	public function admin_columns( $columns ) {

		$columns = array(
			'cb'                     => '<input type = "checkbox" />',
			'title'                  => __( 'Title', 'tcu-timeline-plugin' ),
			'author'                 => __( 'Author', 'tcu-timeline-plugin' ),
			'tcu_timeline_shortcode' => __( 'Shortcode', 'tcu-timeline-plugin' ),
			'date'                   => __( 'Date', 'tcu-timeline-plugin' ),
		);

		return $columns;
	}

	/**
	 * Add timeline shortcode to table in edit.php
	 *
	 * @param  string $column   The name of the column to display.
	 * @param  int    $post_id  The ID of the current post.
	 */
	public function shortcode_column( $column, $post_id ) {
		global $typenow;

		if ( self::$post_type_name === $typenow ) {

			$shortcode = 'tcu_timeline_shortcode';

			if ( $column === $shortcode) {
				echo wp_kses_post( '<code>[tcu_timeline id="' . $post_id . '"]</code>' );

			}
		}
	}
}
